﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;


namespace Stat_Weight_Calc_WPF
{
    public class Get_Values
    {
        public int ClassID { get; set; } //ID for each class,for db version
        public string ClassName { get; set; } //name of the class pulled
        public int SpecID { get; set; } //id for a class's spec,for db version
        public string SpecName { get; set; } //name of a spec
        //all the paramters below are for the value each class gets from a certain stat,some will need some context and comments will be added for them
        public double PrimWeight { get; set; } //A class/spec primay weight,all are treated the same in the formula so no needs to differentiate between the 3
        public double CritWeight { get; set; } 
        public double MastWeight { get; set; }
        public double VersWeight { get; set; }
        public double HasteWeight { get; set; }
        public double LeechWeight { get; set; } //only tank/healer specializations will care about Leech,value will come in as 0 for other specs
        public double StamWeight { get; set; } //only tank specializations will care about Stamina,value will come in as 0 for other specs
        public double ArmorWeight { get; set; } //only tank specializations will care about Armor,value will come in as 0 for other specs


        //Note:The database for this project was lost when my install of windows got corrupted,I was unable to retive the database
        //Recreating it would invole a alot of guesswork(Hard to recall how I had it setup)
        //below is the method that was used to get the data from the database
        public List<Get_Values> getValuesDB()

        {
            List<Get_Values> classSpecs = new List<Get_Values>();//make an empty list to fill the data into and return
            SqlConnection conn = new SqlConnection("Data Source=DESKTOP-9S3H0FR\\SQLEXPRESS;Initial Catalog=statWeightsDB;Integrated Security=True");//create the sql connection
            SqlCommand cmd = new SqlCommand("GetClassSpecInfo", conn); //create a command pointing to the stored produce,give connection to tell it where the database is
            cmd.CommandType = CommandType.StoredProcedure; //tell the command its a stored procedure
            DataTable dt = new DataTable(); //create a datatable to store the data to populate the return list
            SqlDataAdapter da = new SqlDataAdapter(cmd); //create a data adapter for the command
            
            conn.Open(); //onpen the connection
            da.SelectCommand = cmd; //select the command
            da.Fill(dt); //fill the datatable with the command
            conn.Close(); //close off the connection

            //loop though the data table to populate the list
            foreach (DataRow dr in dt.Rows)
            {
                classSpecs.Add(
                    new Get_Values
                    {
                        ClassID = Convert.ToInt32(dr["ClassID"]), 
                        ClassName = Convert.ToString(dr["ClassName"]),
                        SpecID = Convert.ToInt32(dr["SpecID"]),
                        SpecName = Convert.ToString(dr["SpecName"]),
                        PrimWeight = Convert.ToDouble(dr["PrimaryWeight"]),
                        CritWeight = Convert.ToDouble(dr["CritWeight"]),
                        MastWeight = Convert.ToDouble(dr["MastWeight"]),
                        VersWeight = Convert.ToDouble(dr["VersWeight"]),
                        HasteWeight = Convert.ToDouble(dr["HasteWeight"]),
                        //note that in the database,these values were 0 for classes that did not care about them
                        //In a full applcation designed to allow adding for values,would need to ensure that these are always inserted into the database
                        //simply check when adding if a value is provided,if not just set it to zero
                        LeechWeight = Convert.ToDouble(dr["LeechWeight"]),
                        StamWeight = Convert.ToDouble(dr["StamWeight"]),
                        ArmorWeight = Convert.ToDouble(dr["ArmorWeight"])
                    }
                    );
            }



            return classSpecs; //return the list will all the data
        }

        //method to get the specfic values for a specialization based on the class ID
        //this will return all the class's specialization's values
        //once this is loaded up,no need to recall to database until a class is changed
        public List<Get_Values> getSpecValues(int _selectedClass)
        {
            List<Get_Values> specVals = new List<Get_Values>();//create an empty list to store the values and return them
            SqlConnection conn = new SqlConnection("Data Source=DESKTOP-9S3H0FR\\SQLEXPRESS;Initial Catalog=statWeightsDB;Integrated Security=True"); //create the sql connection
            SqlCommand cmd = new SqlCommand("getSpecVals", conn); //create a command pointing to the stored produce,give connection to tell it where the database is
            cmd.CommandType = CommandType.StoredProcedure;//tell the command its a stored procedure
            cmd.Parameters.Add("@classID", SqlDbType.Decimal).Value = _selectedClass; //add the paramter to the command,tell it what paramter it is,what datatype,and set the value 
            DataTable dt = new DataTable(); //create a datatable to store the data to populate the return list
            SqlDataAdapter da = new SqlDataAdapter(cmd); //create a data adapter for the command

            conn.Open(); //onpen the connection
            da.SelectCommand = cmd; //select the command
            da.Fill(dt); //fill the datatable with the command
            conn.Close(); //close off the connection

            //loop though the data table to populate the list
            foreach (DataRow dr in dt.Rows)
            {
                specVals.Add(
                    new Get_Values
                    {     
                        SpecID = Convert.ToInt32(dr["SpecID"]),
                        PrimWeight = Convert.ToDouble(dr["PrimaryWeight"]),
                        CritWeight = Convert.ToDouble(dr["CritWeight"]),
                        MastWeight = Convert.ToDouble(dr["MastWeight"]),
                        VersWeight = Convert.ToDouble(dr["VersWeight"]),
                        HasteWeight = Convert.ToDouble(dr["HasteWeight"]),
                        LeechWeight = Convert.ToDouble(dr["LeechWeight"]),
                        StamWeight = Convert.ToDouble(dr["StamWeight"]),
                        ArmorWeight = Convert.ToDouble(dr["ArmorWeight"])
                    }
                    );
            }

            

            return specVals;
        }

        //simple method that creates a list of class names are returning it
        //rather than hard code the list box,I prefer this method
        //Since I can come back here to add a new class if need be
        //have to use this since I dont have the database anymore
        public List<string> getClassNames()
        {
            List<string> classNames = new List<string>()
        {
            "Warlock",
            "Priest",
            "Mage",
            "Shaman",
            "Hunter",
            "Rouge",
            "Monk",
            "Druid",
            "Demon Hunter",
            "Death Knight",
            "Paladin",
            "Warrior"
        };
            return classNames;
        }

        public List<string> getSpecNames(int _selectedClass)
        {
            //make a list to throw back,list of strings will do since it is for a listbox
            List<string> specVals = new List<string>();

            #region contact db instead
            ////general connection for the database
            //SqlConnection conn = new SqlConnection("Data Source=DESKTOP-9S3H0FR\\SQLEXPRESS;Initial Catalog=statWeightsDB;Integrated Security=True");
            //SqlCommand cmd = new SqlCommand("getSpecNames", conn);
            //cmd.CommandType = CommandType.StoredProcedure; //state this is a proc we are running
            //cmd.Parameters.Add("@classID", SqlDbType.Decimal).Value = _selectedClass; //pass it the parmeter needed
            ////datatable and adpater to hold/read the data
            //DataTable dt = new DataTable();
            //SqlDataAdapter da = new SqlDataAdapter(cmd);

            ////running the proc and filling the datatable,Always open and close the connection!
            //conn.Open();
            //da.SelectCommand = cmd;
            //da.Fill(dt);
            //conn.Close();

            ////goes though the datatable and adds the names to the list,
            //foreach (DataRow dr in dt.Rows)
            //{ specVals.Add(Convert.ToString(dr["SpecName"])); 
                  
                
            //}
            #endregion

            List<Get_Values> all = getValuesDB();
            //linq query to get all the specializations tied to a class
            var query = from n in all
            where Convert.ToInt32(n.ClassID) == _selectedClass
            select n.SpecName;

            specVals = query.ToList<string>();//fill the list with the data from the query
            
            return specVals;//return it 

            
        }

        public List<Get_Values> getStatWeights(int _selectedClass,string _className)
        {
           
            List<Get_Values> statWeights = new List<Get_Values>();//create a list to populate and return
            SqlConnection conn = new SqlConnection("Data Source=DESKTOP-9S3H0FR\\SQLEXPRESS;Initial Catalog=statWeightsDB;Integrated Security=True");//create a connection
            SqlCommand cmd = new SqlCommand("getStatWeights");//create a command 
            cmd.Connection = conn; //telling it what connection we want to use
            cmd.CommandType = CommandType.StoredProcedure; //state what command we want to run
            //passing the paremnters needed
            cmd.Parameters.Add("@classID", SqlDbType.Decimal).Value = _selectedClass;
            cmd.Parameters.Add("@specName", SqlDbType.VarChar).Value = _className;   
            //datatable and adpater for the data to be stored in/read with      
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            //acualty exucting the query,make sure to open and then close the connection
            conn.Open();
            da.SelectCommand = cmd;
            da.Fill(dt);
            conn.Close();

            //goes though the datatable and adds a item in the list with the values
            foreach (DataRow dr in dt.Rows)
            {
                statWeights.Add(
                     new Get_Values
                     {
                         SpecID = Convert.ToInt32(dr["SpecID"]),
                         PrimWeight = Convert.ToDouble(dr["PrimaryWeight"]),
                         CritWeight = Convert.ToDouble(dr["CritWeight"]),
                         MastWeight = Convert.ToDouble(dr["MastWeight"]),
                         VersWeight = Convert.ToDouble(dr["VersWeight"]),
                         HasteWeight = Convert.ToDouble(dr["HasteWeight"]),
                         LeechWeight = Convert.ToDouble(dr["LeechWeight"]),
                         StamWeight = Convert.ToDouble(dr["StamWeight"]),
                         ArmorWeight = Convert.ToDouble(dr["ArmorWeight"])
                     }


                     );


            }

            return statWeights;
        }


        //old hardcoded version before I set up the database
        //Terrible idea in real world use,since every time there is a update to any Class/Specialization this has to be changed
        //Glad I left this in however,since the database is gone
        public double[] getClassSpecCombo(int _class, int _spec)
        {

            double[] StatWeights = new double[8]; //create an empty array,size 8 includes all stats

            //This is all the data for each class and there specializations
            //switch could work aswell,prehaps better than a really long if statment
            #region classspec statweight Data
            //WarLock
            if (_class == 0) //first we need to know what class we are looking at
            {
                if (_spec == 0) //then find the spec
                {
                    //Aff lock
                    //then we need to fill out the array with that specialization's stat weights
                    StatWeights[0] = 12.94; 
                    StatWeights[1] = 10.92; 
                    StatWeights[2] = 9.28;
                    StatWeights[3] = 9.25;
                    StatWeights[4] = 12.39;
                    StatWeights[6] = 0;
                    StatWeights[6] = 0;
                    StatWeights[7] = 0;
                }

                if (_spec == 1)
                {
                    //Demon Lock
                    StatWeights[0] = 10.31;
                    StatWeights[1] = 8.01;
                    StatWeights[2] = 5.74;
                    StatWeights[3] = 7.78;
                    StatWeights[4] = 14.33;
                    StatWeights[6] = 0;
                    StatWeights[6] = 0;
                    StatWeights[7] = 0;
                }

                if (_spec == 2)
                {
                    //Destro Lock
                    StatWeights[0] = 15.41;
                    StatWeights[1] = 12.33;
                    StatWeights[2] = 13.51;
                    StatWeights[3] = 10.89;
                    StatWeights[4] = 15.57;
                    StatWeights[6] = 0;
                    StatWeights[6] = 0;
                    StatWeights[7] = 0;
                }
            }

            //Priest
            if (_class == 1)
            {
                if (_spec == 0)
                {
                    //Shadow
                    StatWeights[0] = 13.72;
                    StatWeights[1] = 14.72;
                    StatWeights[2] = 14.5;
                    StatWeights[3] = 9.37;
                    StatWeights[4] = 17.39;
                    StatWeights[6] = 0;
                    StatWeights[6] = 0;
                    StatWeights[7] = 0;
                }

                if (_spec == 1)
                {
                    //Holy
                    StatWeights[0] = 14.54;
                    StatWeights[1] = 14.38;
                    StatWeights[2] = 12.22;
                    StatWeights[3] = 10.08;
                    StatWeights[4] = 4.08;
                    StatWeights[6] = 4.48;
                    StatWeights[6] = 0;
                    StatWeights[7] = 0;
                }


                if (_spec == 2)
                {
                    //Disc 
                    StatWeights[0] = 11.89;
                    StatWeights[1] = 8.6;
                    StatWeights[2] = 4.88;
                    StatWeights[3] = 7.88;
                    StatWeights[4] = 6.57;
                    StatWeights[6] = 7.95;
                    StatWeights[6] = 0;
                    StatWeights[7] = 0;
                }
            }

            //mage
            if (_class == 2)
            {
                if (_spec == 0)
                {
                    //Arcane
                    StatWeights[0] = 12.25;
                    StatWeights[1] = 9.41;
                    StatWeights[2] = 8.27;
                    StatWeights[3] = 8.59;
                    StatWeights[4] = 8.84;
                    StatWeights[6] = 0;
                    StatWeights[6] = 0;
                    StatWeights[7] = 0;
                }

                if (_spec == 1)
                {
                    //Fire
                    StatWeights[0] = 16.74;
                    StatWeights[1] = 12.68;
                    StatWeights[2] = 13.13;
                    StatWeights[3] = 11.43;
                    StatWeights[4] = 12.22;
                    StatWeights[6] = 0;
                    StatWeights[6] = 0;
                    StatWeights[7] = 0;
                }

                if (_spec == 2)
                {
                    //Frost
                    StatWeights[0] = 14.95;
                    StatWeights[1] = 12.07;
                    StatWeights[2] = 5.28;
                    StatWeights[3] = 10.05;
                    StatWeights[4] = 17.26;
                    StatWeights[6] = 0;
                    StatWeights[6] = 0;
                    StatWeights[7] = 0;
                }
            }

            //shaman
            if (_class == 3)
            {

                if (_spec == 0)
                {
                    //Ele Shaman
                    StatWeights[0] = 15.04;
                    StatWeights[1] = 17.66;
                    StatWeights[2] = 7.28;
                    StatWeights[3] = 10.25;
                    StatWeights[4] = 15.62;
                    StatWeights[6] = 0;
                    StatWeights[6] = 0;
                    StatWeights[7] = 0;
                }

                if (_spec == 1)
                {
                    //Enhance Shaman
                    StatWeights[0] = 13.91;
                    StatWeights[1] = 8.63;
                    StatWeights[2] = 11.48;
                    StatWeights[3] = 8.06;
                    StatWeights[4] = 10.37;
                    StatWeights[6] = 0;
                    StatWeights[6] = 0;
                    StatWeights[7] = 0;
                }

                if (_spec == 2)
                {
                    //Resto Shaman 
                    StatWeights[0] = 12.51;
                    StatWeights[1] = 12.46;
                    StatWeights[2] = 9.84;
                    StatWeights[3] = 8.81;
                    StatWeights[4] = 6.06;
                    StatWeights[6] = 3.34;
                    StatWeights[6] = 0;
                    StatWeights[7] = 0;
                }
            }

            //Hunter
            if (_class == 4)
            {
                if (_spec == 0)
                {
                    //BM
                    StatWeights[0] = 17.24;
                    StatWeights[1] = 10.25;
                    StatWeights[2] = 13.53;
                    StatWeights[3] = 9.14;
                    StatWeights[4] = 11.86;
                    StatWeights[6] = 0;
                    StatWeights[6] = 0;
                    StatWeights[7] = 0;
                }

                if (_spec == 1)
                {
                    //Marksman
                    StatWeights[0] = 12.9;
                    StatWeights[1] = 10.46;
                    StatWeights[2] = 15.16;
                    StatWeights[3] = 8.48;
                    StatWeights[4] = 10.68;
                    StatWeights[6] = 0;
                    StatWeights[6] = 0;
                    StatWeights[7] = 0;
                }

                if (_spec == 2)
                {
                    //Surv 
                    StatWeights[0] = 16.79;
                    StatWeights[1] = 10.12;
                    StatWeights[2] = 1.38;
                    StatWeights[3] = 9.99;
                    StatWeights[4] = 8.88;
                    StatWeights[6] = 0;
                    StatWeights[6] = 0;
                    StatWeights[7] = 0;
                }

            }

            //rouge
            if (_class == 5)
            {

                if (_spec == 0)
                {
                    //Assasination 
                    StatWeights[0] = 17.43;
                    StatWeights[1] = 11.33;
                    StatWeights[2] = 11.47;
                    StatWeights[3] = 8.9;
                    StatWeights[4] = 5.34;
                    StatWeights[6] = 0;
                    StatWeights[6] = 0;
                    StatWeights[7] = 0;
                }
                if (_spec == 1)
                {
                    //OutLaw 
                    StatWeights[0] = 18.81;
                    StatWeights[1] = 10.36;
                    StatWeights[2] = 11.43;
                    StatWeights[3] = 10.14;
                    StatWeights[4] = 9.76;
                    StatWeights[6] = 0;
                    StatWeights[6] = 0;
                    StatWeights[7] = 0;
                }
                if (_spec == 2)
                {
                    //Sub 
                    StatWeights[0] = 17.02;
                    StatWeights[1] = 9.31;
                    StatWeights[2] = 11.55;
                    StatWeights[3] = 9.06;
                    StatWeights[4] = 4.47;
                    StatWeights[6] = 0;
                    StatWeights[6] = 0;
                    StatWeights[7] = 0;
                }

            }

            //monk
            if (_class == 6)
            {
                if (_spec == 0)
                {
                    //Windwalker
                    StatWeights[0] = 19.72;
                    StatWeights[1] = 10.7;
                    StatWeights[2] = 10.43;
                    StatWeights[3] = 9.9;
                    StatWeights[4] = 8.24;
                    StatWeights[6] = 0;
                    StatWeights[6] = 0;
                    StatWeights[7] = 0;
                }

                if (_spec == 1)
                {
                    //Brewmaster
                    StatWeights[0] = 7.03;
                    StatWeights[1] = 7.47;
                    StatWeights[2] = 10.09;
                    StatWeights[3] = 11.06;
                    StatWeights[4] = 6.36;
                    StatWeights[6] = 14.87;
                    StatWeights[6] = 13.57;
                    StatWeights[7] = 52.17;
                }

                if (_spec == 2)
                {
                    //Mistweaver
                    StatWeights[0] = 12.31;
                    StatWeights[1] = 9.1;
                    StatWeights[2] = 8.19;
                    StatWeights[3] = 8.47;
                    StatWeights[4] = 6.49;
                    StatWeights[6] = 3.98;
                    StatWeights[6] = 0;
                    StatWeights[7] = 0;
                }

            }

            //druid
            if (_class == 7)
            {
                if (_spec == 0)
                {
                    //Balance 
                    StatWeights[0] = 14.19;
                    StatWeights[1] = 10.32;
                    StatWeights[2] = 12.74;
                    StatWeights[3] = 9.59;
                    StatWeights[4] = 12.72;
                    StatWeights[6] = 0;
                    StatWeights[6] = 0;
                    StatWeights[7] = 0;
                }

                if (_spec == 1)
                {
                    //Feral 
                    StatWeights[0] = 12.53;
                    StatWeights[1] = 7.41;
                    StatWeights[2] = 6.67;
                    StatWeights[3] = 6.85;
                    StatWeights[4] = 5.81;
                    StatWeights[6] = 0;
                    StatWeights[6] = 0;
                    StatWeights[7] = 0;
                }

                if (_spec == 2)
                {
                    //Resto 
                    StatWeights[0] = 12.96;
                    StatWeights[1] = 9.81;
                    StatWeights[2] = 15.14;
                    StatWeights[3] = 8.91;
                    StatWeights[4] = 8.19;
                    StatWeights[6] = 4.86;
                    StatWeights[6] = 0;
                    StatWeights[7] = 0;

                }

                if (_spec == 3)
                {
                    //Guardian
                    StatWeights[0] = 2.84;
                    StatWeights[1] = 3.88;
                    StatWeights[2] = 11.13;
                    StatWeights[3] = 11.89;
                    StatWeights[4] = 5.65;
                    StatWeights[6] = 17.14;
                    StatWeights[6] = 15.41;
                    StatWeights[7] = 106.39;

                }

            }

            //demon hunter

            if (_class == 8)
            {
                if (_spec == 0)
                {
                    //Havoc
                    StatWeights[0] = 15.32;
                    StatWeights[1] = 9.25;
                    StatWeights[2] = 7.68;
                    StatWeights[3] = 7.40;
                    StatWeights[4] = 7.26;
                    StatWeights[6] = 0;
                    StatWeights[6] = 0;
                    StatWeights[7] = 0;
                }

                if (_spec == 1)
                {
                    //Vengenace here
                    StatWeights[0] = 9.13;
                    StatWeights[1] = 7.65;
                    StatWeights[2] = 9.09;
                    StatWeights[3] = 11.15;
                    StatWeights[4] = 5.95;
                    StatWeights[6] = 14.44;
                    StatWeights[6] = 103.39;
                    StatWeights[7] = 57.74;
                }

            }

            //dk
            if (_class == 9)
            {
                if (_spec == 0)
                {
                    //frost
                    StatWeights[0] = 16.15;
                    StatWeights[1] = 16.57;
                    StatWeights[2] = 12.4;
                    StatWeights[3] = 9.23;
                    StatWeights[4] = 10.36;
                    StatWeights[6] = 0;
                    StatWeights[6] = 0;
                    StatWeights[7] = 0;
                }

                if (_spec == 1)
                {
                    //unholy
                    StatWeights[0] = 15.48;
                    StatWeights[1] = 9.78;
                    StatWeights[2] = 12.34;
                    StatWeights[3] = 8.7;
                    StatWeights[4] = 9.05;
                    StatWeights[6] = 0;
                    StatWeights[6] = 0;
                    StatWeights[7] = 0;
                }

                if (_spec == 2)
                {
                    //Blood 
                    StatWeights[0] = 5.65;
                    StatWeights[1] = 7.3;
                    StatWeights[2] = 3.91;
                    StatWeights[3] = 12.4;
                    StatWeights[4] = 4.36;
                    StatWeights[6] = 10.57;
                    StatWeights[6] = 11.84;
                    StatWeights[7] = 50.36;
                }

            }

            //paladin
            if (_class == 10)
            {
                if (_spec == 0)
                {
                    //reti
                    StatWeights[0] = 14.27;
                    StatWeights[1] = 10.51;
                    StatWeights[2] = 4.23;
                    StatWeights[3] = 9.19;
                    StatWeights[4] = 12.18;
                    StatWeights[6] = 0;
                    StatWeights[6] = 0;
                    StatWeights[7] = 0;
                }

                if (_spec == 1)
                {
                    //Holy
                    StatWeights[0] = 11.29;
                    StatWeights[1] = 9.26;
                    StatWeights[2] = 7.97;
                    StatWeights[3] = 6.76;
                    StatWeights[4] = 2.92;
                    StatWeights[6] = 2.07;
                    StatWeights[6] = 0;
                    StatWeights[7] = 0;
                }

                if (_spec == 2)
                {
                    //Protection
                    StatWeights[0] = 3.52;
                    StatWeights[1] = 8;
                    StatWeights[2] = 8.49;
                    StatWeights[3] = 9.25;
                    StatWeights[4] = 10.8;
                    StatWeights[6] = 7.87;
                    StatWeights[6] = 11.8;
                    StatWeights[7] = 42.72;
                }

            }

            //warrior
            if (_class == 11)
            {
                if (_spec == 0)
                {
                    //arms
                    StatWeights[0] = 11.76;
                    StatWeights[1] = 9.55;
                    StatWeights[2] = 10.27;
                    StatWeights[3] = 9.24;
                    StatWeights[4] = 13.27;
                    StatWeights[6] = 0;
                    StatWeights[6] = 0;
                    StatWeights[7] = 0;
                }
                if (_spec == 1)
                {
                    //fury
                    StatWeights[0] = 9.68;
                    StatWeights[1] = 7.19;
                    StatWeights[2] = 6.29;
                    StatWeights[3] = 7.53;
                    StatWeights[4] = 10;
                    StatWeights[6] = 0;
                    StatWeights[6] = 0;
                    StatWeights[7] = 0;
                }

                if (_spec == 2)
                {
                    //prot here
                    StatWeights[0] = 14.16;
                    StatWeights[1] = 4;
                    StatWeights[2] = 12.99;
                    StatWeights[3] = 15.92;
                    StatWeights[4] = 3.74;
                    StatWeights[6] = 19.91;
                    StatWeights[6] = 9.17;
                    StatWeights[7] = 26.82;
                }
                #endregion


            }
            return StatWeights; //once we have the data we want,return the array
        }

        //old hardcoded method to get the specializations for a class
        public string[] GetSpecs(int classSelected)
        {
            //Since the maximium of specializations is 4(Druid class has 4,demon hunters have 2,rest have 3)
            string[] classArray = new string[4];

            //look for the index of the class
            if (classSelected == 0) //when we hit the class we want,fill the array with the data
            {
                //Warlock
                classArray[0] = "Affliction";
                classArray[1] = "Demononolgy";
                classArray[2] = "Destruction";

            }

            if (classSelected == 1)
            {
                //priest
                classArray[0] = "Shadow";
                classArray[1] = "Holy";
                classArray[2] = "Discpline";

            }

            if (classSelected == 2)
            {
                //mage
                classArray[0] = "Arcane";
                classArray[1] = "Fire";
                classArray[2] = "Frost";

            }

            if (classSelected == 3)
            {
                //shaman
                classArray[0] = "Elemental";
                classArray[1] = "Enhancement";
                classArray[2] = "Restoration";

            }

            if (classSelected == 4)
            {

                //hunter
                classArray[0] = "Beast Mastery";
                classArray[1] = "MarksMan";
                classArray[2] = "Survial";

            }

            if (classSelected == 5)
            {
                //rouge
                classArray[0] = "Assasination";
                classArray[1] = "Outlaw";
                classArray[2] = "Subelty";

            }

            if (classSelected == 6)
            {
                //monk
                classArray[0] = "Windwalker";
                classArray[1] = "BrewMaster";
                classArray[2] = "MistWeaver";

            }

            if (classSelected == 7)
            {
                //druid
                classArray[0] = "Balance";
                classArray[1] = "Feral";
                classArray[2] = "Restroation";
                classArray[3] = "Guardain";

            }

            if (classSelected == 8)
            {
                //demonhunter
                classArray[0] = "Havoc";
                classArray[1] = "Vengance";

            }

            if (classSelected == 9)
            {
                //death knight
                classArray[0] = "Frost";
                classArray[1] = "Unholy";
                classArray[2] = "Blood";

            }

            if (classSelected == 10)
            {
                //paladin
                classArray[0] = "Retribution";
                classArray[1] = "Holy";
                classArray[2] = "Protection";

            }

            if (classSelected == 11)
            {
                //warrior
                classArray[0] = "Arms";
                classArray[1] = "Fury";
                classArray[2] = "Protection";

            }
            return classArray; //then return the array

        }

    }
}
