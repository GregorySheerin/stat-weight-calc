﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Stat_Weight_Calc_WPF;



namespace WpfApplication1
{
    public partial class MainWindow : Window
    {
        
        public MainWindow()
        {  
            InitializeComponent();
            Get_Values getvalsInstance = new Get_Values(); //instance to the Get Values Class

            #region Database Version
            //loop though the classSpecData list,and get all the class names into the class name list
            //propably a better way to go about this,I want to ensure that I just get a list of Class names
            // List<Get_Values> classSpecData = getvalsInstance.getValuesDB(); //call the spec data in from the database
            //foreach (var item in classSpecData)
            //{
            //    classNames.Add(item.ClassName);
            //}
            //tell the list only to count distinct class names(to ensure we dont have the same name twice)
            // classNames = classNames.Distinct().ToList();
            #endregion

            List<string> classNames = getvalsInstance.getClassNames(); //create a list and make it equal the list from the Get values class
            //then add all of these to the Class list on the UI
            foreach (var item in classNames)
            {
                ClassLST.Items.Add(item);
            }
            //this can be done on start up,since the list of Classes will only change if a new class is added to the game
        }

        //instance of class containing methods

        Get_Values getvalsInstance = new Get_Values(); //another instance of the Get Values Class
       
        private void button_Click(object sender, RoutedEventArgs e)
        {
            //two vals for the current and new item
            double CurrentVal = 0;
            double NewVal = 0;
            //storing weights in arrays,all values are doubles and is neater,althought with knowing what order they come in can become confusing
            double[] CurrentStats = new double[8];
            double[] NewStats = new double[8];

            //try to pull out the data from the Textboxes for the Stats of the two items we are comparing
            try
            {
                //State for the item the player currenty has
                CurrentStats[0] = double.Parse(PrimCurTxt.Text.ToString());
                CurrentStats[1] = double.Parse(CritCurTxt.Text.ToString());
                CurrentStats[2] = double.Parse(MastCurTxt.Text.ToString());
                CurrentStats[3] = double.Parse(VersCurTxt.Text.ToString());
                CurrentStats[4] = double.Parse(HasteCurTxt.Text.ToString());
                CurrentStats[5] = double.Parse(LeechCurTxt.Text.ToString());
                CurrentStats[6] = double.Parse(StamCurTxt.Text.ToString());
                CurrentStats[7] = double.Parse(ArmorCurTxt.Text.ToString());

                //Stats for the item the player is upgrading to
                NewStats[0] = double.Parse(PrimNewTxt.Text.ToString());
                NewStats[1] = double.Parse(CritNewTxt.Text.ToString());
                NewStats[2] = double.Parse(MastNewTxt.Text.ToString());
                NewStats[3] = double.Parse(VersNewTxt.Text.ToString());
                NewStats[4] = double.Parse(HasteNewTxt.Text.ToString());
                NewStats[5] = double.Parse(LeechNewTxt.Text.ToString());
                NewStats[6] = double.Parse(StamnewTxt.Text.ToString());
                NewStats[7] = double.Parse(ArmorNewTxt.Text.ToString());
            }

            //if there is an error,display it in a label so I can know(mainly for debugging,not needed in a real product)
            catch (Exception err)
            {
                Errlbl.Content = err.Message;

            }

            int selectedClass = ClassLST.SelectedIndex; //get what clas we are selected
            int selectedSpec = SpecSelectLSB.SelectedIndex;  //get what specialization we are selecting

            //make an array to store the stat weights
            double[] StatWeights = new double[8];

            //since we load to weights into labels so the user can see them,we can rip them straight out of the labels rather than going back to the database
            StatWeights[0] = Convert.ToDouble(PrimWeightLbl.Content);
            StatWeights[1] = Convert.ToDouble(CritWeightLbl.Content);
            StatWeights[2] = Convert.ToDouble(MastWeightLlb.Content);
            StatWeights[3] = Convert.ToDouble(VersWeightLbl.Content);
            StatWeights[4] = Convert.ToDouble(HasteWeightLbl.Content);
            StatWeights[5] = Convert.ToDouble(LeechWeightLbl.Content);
            StatWeights[6] = Convert.ToDouble(StamWeightLbl.Content);
            StatWeights[7] = Convert.ToDouble(ArmWeightLbl.Content);

            //actual formula to calucate the value of an item
            //Every stat is mutiplied by the weight of the stat,add all these numbers up to get the "value" of an item for a player
            //EG (XStat * XWeight) gets the value of X of that item
            CurrentVal = (CurrentStats[0] * StatWeights[0]) + (CurrentStats[1] * StatWeights[1]) + (CurrentStats[2] * StatWeights[2]) + (CurrentStats[3] * StatWeights[3]) + (CurrentStats[4] * StatWeights[4]) + (CurrentStats[5] * StatWeights[5]) + (CurrentStats[6] * StatWeights[6]) + (CurrentStats[7] * StatWeights[7]);
            NewVal = (NewStats[0] * StatWeights[0]) + (NewStats[1] * StatWeights[1]) + (NewStats[2] * StatWeights[2]) + (NewStats[3] * StatWeights[3]) + (NewStats[4] * StatWeights[4]) + (NewStats[5] * StatWeights[5]) + (NewStats[6] * StatWeights[6]) + (NewStats[7] * StatWeights[7]);

            //finaly display the values to the user
            CurrVallbl.Content = CurrentVal.ToString();
            NewVallbl.Content = NewVal.ToString();
        }

        //method to display the Stat weight information to the user
        //also using this to get the weights for the calcuation
        private void ClassLST_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            #region Database Version
            //Make sure we always clear the specs from the listbox before we load in more
            //SpecSelectLSB.Items.Clear();

            ////adding +1 since database ids start at one,while List boxe's index start at 0
            //int selectedClass = ClassLST.SelectedIndex + 1;

            ////get the list of spec names from the database
            //List<string> specData = getvalsInstance.getSpecNames(selectedClass);

            ////loop though the
            //foreach (var item in specData)
            //{
            //    SpecSelectLSB.Items.Add(item);
            //}
            #endregion

            //simliar to above but for none database version
            #region Hardcoded Verion
            SpecSelectLSB.Items.Clear();
            int selected = ClassLST.SelectedIndex;
            string[] classArray = new string[4];
            classArray = getvalsInstance.GetSpecs(selected);

           // class 7 is Druid,which has 4 specs,not 3,to account for this make sure we check before we go in
            if (selected != 7)
            {
                SpecSelectLSB.Items.Add(classArray[0]);
                SpecSelectLSB.Items.Add(classArray[1]);
                SpecSelectLSB.Items.Add(classArray[2]);
            }

            else
            {
                SpecSelectLSB.Items.Add(classArray[0]);
                SpecSelectLSB.Items.Add(classArray[1]);
                SpecSelectLSB.Items.Add(classArray[2]);
                SpecSelectLSB.Items.Add(classArray[3]);
            }
            #endregion




        }

        private void SpecSelectLSB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Here is where the stat weights are loaded to labels to show the user the stats for the current class+spec there are calculating
            //Also using the labels to get the values for the calculation

            #region Database Version
            // //accounting for index starting at 0 and the database IDs starting at 1
            // int selectedClass = ClassLST.SelectedIndex + 1;
            // string selectedSpec = SpecSelectLSB.SelectedItem.ToString();
            // //"List" that is returned for the database,contains only one item,want to keep it as list incase of same specializations having other builds
            // //Some specializations can have two builds based on player's character customization
            // List<Get_Values> selectedStatWeights = getvalsInstance.getStatWeights(selectedClass,selectedSpec);


            ////since there is only one item,.first() will get use there rather than a for each loop,(Foreach will be needed for muilt build specs,as well as a tab for the labels)
            // PrimWeightLbl.Content = selectedStatWeights.First().PrimWeight;
            // CritWeightLbl.Content = selectedStatWeights.First().CritWeight;
            // MastWeightLlb.Content = selectedStatWeights.First().MastWeight;
            // VersWeightLbl.Content = selectedStatWeights.First().VersWeight;
            // HasteWeightLbl.Content = selectedStatWeights.First().HasteWeight;
            // LeechWeightLbl.Content = selectedStatWeights.First().LeechWeight;
            // StamWeightLbl.Content = selectedStatWeights.First().StamWeight;
            // ArmWeightLbl.Content = selectedStatWeights.First().ArmorWeight;
            #endregion 


            #region HardCoded Version
          //  since we are not counting for the database here, no need to + 1 this
            int selectedClass = ClassLST.SelectedIndex;
            int selectedspec = SpecSelectLSB.SelectedIndex;

          //  create an array to store the stat weights
            double[] StatWeights = new double[8];

           // call the method in Get values Class to return the data for a spec
            StatWeights = getvalsInstance.getClassSpecCombo(selectedClass, selectedspec);

          //  fill the data into the labels on the UI
            PrimWeightLbl.Content = StatWeights[0];
            CritWeightLbl.Content = StatWeights[1];
            MastWeightLlb.Content = StatWeights[2];
            VersWeightLbl.Content = StatWeights[3];
            HasteWeightLbl.Content = StatWeights[4];
            LeechWeightLbl.Content = StatWeights[5];
            StamWeightLbl.Content = StatWeights[6];
            ArmWeightLbl.Content = StatWeights[7];
            #endregion



        }


    }
}
